#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

class CrossScene;
class QGraphicsView;


class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = 0);
    ~MainWindow(){}

private:
    CrossScene *scene;
    QGraphicsView *view;

};

#endif // MAINWINDOW_H
