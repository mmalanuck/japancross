#include "boxitem.h"
#include "crossscene.h"
#include "dataheader.h"
#include "mainwindow.h"
#include "myview.h"

#include <QGraphicsView>
#include <QLayout>
#include <QApplication>


MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
{
    scene = new CrossScene();
//    BoxItem *box = new BoxItem();
    //    scene->addItem(box);

    view = new MyView();
    view->setScene(scene);
    view->setSceneRect(scene->sceneRect());
    view->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);

    auto data = new DataHeader();
    scene->setData(data);
    scene->buildUI();

    scene->setSceneRect(scene->itemsBoundingRect());
    //    view->setViewportUpdateMode(QGraphicsView::FullViewportUpdate);
    //    showFullScreen();
    setCentralWidget(view);
}


