#include "boxitem.h"
#include "crossscene.h"
#include "dataheader.h"

#include <QDebug>

CrossScene::CrossScene(QObject *parent):QGraphicsScene(parent)
{
    pen.setColor(Qt::black);
    pen.setWidth(2);

    thickPen.setColor(QColor(128,128,128,128));
    thickPen.setWidth(1);
}

CrossScene::~CrossScene()
{

}

void CrossScene::setData(DataHeader *data)
{
 _header = data;
}

void CrossScene::drawHeaderBackground(int rectSize)
{
    int maxRows = _header->rowsSize();
    int maxCols = _header->colsSize();

    addRect(0,-maxCols*rectSize, _header->_cols.length()*rectSize, (maxRows+1)*rectSize, pen, Qt::lightGray);
    addRect(-maxRows*rectSize,0, maxRows*rectSize, _header->_rows.length()*rectSize, pen, Qt::lightGray);
}

void CrossScene::drawHeaderCols(int rectSize)
{
    int x = 0;
    int y = -1;

    for(QList<int> col: _header->_cols){
        y = -1;

        for(int num: col){
            BoxItem* item = new BoxItem(num, x*rectSize, y*rectSize, rectSize);
            this->addItem(item);
            y--;
        }

        x++;
    }
}

void CrossScene::drawHeaderRows(int rectSize)
{
    int x = -1;
    int y = 0;

    int maxRows = _header->rowsSize();
    int maxCols = _header->colsSize();

    for(QList<int> row: _header->_rows){
        x = -1;

        for(int num: row){
            BoxItem* item = new BoxItem(num, x*rectSize, y*rectSize, rectSize);
            this->addItem(item);
//            this->addRect((x-1)*rectSize,y*rectSize, rectSize,rectSize,Qt::SolidLine, Qt::gray);
            x--;
        }

        y++;
    }
}

void CrossScene::drawGrids(int rectSize)
{
    int rowCount = _header->_rows.length();
    int colCount = _header->_cols.length();

    for(int i =0; i < rowCount; i++){
        addLine(0, i*rectSize, colCount * rectSize, i*rectSize, thickPen);
    }

    for(int i =0; i < colCount; i++){
        addLine(i*rectSize,0, i*rectSize, rowCount * rectSize, thickPen);
    }
}

void CrossScene::makeHeader(int rectSize)
{
    drawHeaderBackground(rectSize);
    drawHeaderCols(rectSize);
    drawHeaderRows(rectSize);
}

void CrossScene::buildUI()
{
    int rectSize = 16;
    makeHeader(rectSize);
    drawGrids(rectSize);
    drawLines(rectSize);
}

void CrossScene::drawLines(int rectSize)
{
    int maxRows = _header->rowsSize();
    int maxCols = _header->colsSize();

    int rowCounts =_header->_rows.length();
    int colCounts =_header->_cols.length();


    for (int i=0; i< colCounts; i+=5){
        this->addLine(i*rectSize,-(maxCols)*rectSize, i*rectSize, (rowCounts) * rectSize, pen);
    }
    this->addLine(colCounts*rectSize,-(maxCols)*rectSize, colCounts*rectSize, (rowCounts) * rectSize, pen);

    for (int i=0; i< rowCounts; i+=5){
        this->addLine(-(maxRows)*rectSize,i*rectSize,(colCounts)*rectSize,i*rectSize, pen);
    }
    this->addLine(-(maxRows)*rectSize,rowCounts*rectSize,(colCounts)*rectSize,rowCounts*rectSize, pen);
}

