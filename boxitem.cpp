#include "boxitem.h"

#include <QPainter>
#include <QStyleOptionGraphicsItem>
#include <QTextOption>

BoxItem::BoxItem(int value, qreal x, qreal y, qreal size, QGraphicsItem *parent):QGraphicsItem(parent)
{
    _value = value;
    _size = size;

    _rect.setLeft(x);
    _rect.setTop(y);
    _rect.setWidth(size);
    _rect.setHeight(size);

    setFlag(QGraphicsItem::ItemIsSelectable, true);
    setFlag(QGraphicsItem::ItemIsFocusable, true);
}

BoxItem::~BoxItem()
{
}

QRectF BoxItem::boundingRect() const{
    return _rect;
}

void BoxItem::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    Q_UNUSED(widget);


    QRect r = _rect;
    painter->fillRect(r, Qt::lightGray);
    r.adjust(1,1,-1,-1);

    if (option->state & QStyle::State_Selected)
        painter->fillRect(r, Qt::red);
    else
        painter->fillRect(r, Qt::gray);


    QTextOption textOption;
    textOption.setAlignment(Qt::AlignCenter);
    painter->drawText(boundingRect(), QString::number(_value),textOption);}

