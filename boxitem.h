#ifndef BOX_H
#define BOX_H

#include <QGraphicsItem>

class QPainter;
class QStyleOptionGraphicsItem;
class QWidget;

class BoxItem: public QGraphicsItem
{    
public:
    BoxItem(int value, qreal x, qreal y, qreal size, QGraphicsItem *parent = nullptr);
    ~BoxItem();

    qreal size() const{
        return _size;
    }

    void setSize(const qreal size){
        if (size == _size)
            return;

        prepareGeometryChange();
        _size = size;
    }

    QRectF boundingRect() const;
    void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget);

private:    
    int _value=0;
    qreal _size = 25;
    QRect _rect;

};

#endif // BOX_H
