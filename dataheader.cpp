#include "dataheader.h"
#include <QList>
#include <QDebug>

DataHeader::DataHeader(QObject *parent) : QObject(parent)
{

    //https://www.jcross-world.ru/#!jcrosswords/11672

    _cols << QList<int>{2,};
    _cols << QList<int>{3,};
    _cols << QList<int>{3,};
    _cols << QList<int>{5,};
    _cols << QList<int>{6,};

    _cols <<QList<int>{7,};
    _cols <<QList<int>{8,};
    _cols <<QList<int>{10,};
    _cols <<QList<int>{3, 11};
    _cols <<QList<int>{8, 13};

    _cols <<QList<int>{1,12, 14};
    _cols <<QList<int>{1, 14, 16};
    _cols <<QList<int>{1, 7, 5, 17};
    _cols <<QList<int>{2, 3, 6, 5, 16};
    _cols <<QList<int>{2, 6, 5, 4, 17};

    _cols <<QList<int>{12, 5, 4, 17};
    _cols <<QList<int>{13, 4, 4, 17};
    _cols <<QList<int>{6, 6, 4, 6, 17,};
    _cols <<QList<int>{5, 11, 7, 18,};
    _cols <<QList<int>{4, 10, 10, 16,};

    _cols <<QList<int>{4, 10, 6, 16};
    _cols <<QList<int>{4,10,16};
    _cols <<QList<int>{4,3, 12, 14};
    _cols <<QList<int>{4, 19, 14};
    _cols <<QList<int>{40,};

    _cols <<QList<int>{7,30};
    _cols <<QList<int>{6, 27};
    _cols <<QList<int>{6, 26};
    _cols <<QList<int>{5, 26};
    _cols <<QList<int>{4, 28};

    _cols <<QList<int>{4, 32};
    _cols <<QList<int>{4,36};
    _cols <<QList<int>{4, 41};
    _cols <<QList<int>{4, 45};
    _cols <<QList<int>{4, 5, 40};

    _cols <<QList<int>{3, 4, 40};
    _cols <<QList<int>{3, 39};
    _cols <<QList<int>{3, 10, 8, 17};
    _cols <<QList<int>{2, 11, 8, 16};
    _cols <<QList<int>{2, 10, 4, 14};

    _cols <<QList<int>{2, 8, 10};
    _cols <<QList<int>{1, 9, 5,};
    _cols <<QList<int>{1, 7, 2,};
    _cols <<QList<int>{4,};

    _rows << QList<int>{6,};
    _rows << QList<int>{7};
    _rows << QList<int>{7,7};
    _rows << QList<int>{8,12};
    _rows << QList<int>{9,14};

    _rows << QList<int>{8,17};
    _rows << QList<int>{9,19};
    _rows << QList<int>{9,19};
    _rows << QList<int>{9,17};
    _rows << QList<int>{9,15};

    _rows << QList<int>{9,14};
    _rows << QList<int>{9,14};
    _rows << QList<int>{9,14};
    _rows << QList<int>{10,13};
    _rows << QList<int>{9,14};

    _rows << QList<int>{9,13};
    _rows << QList<int>{8,13};
    _rows << QList<int>{8,13};
    _rows << QList<int>{8,13};
    _rows << QList<int>{8,11};

    _rows << QList<int>{9,10};
    _rows << QList<int>{10,9};
    _rows << QList<int>{21,};
    _rows << QList<int>{21,};
    _rows << QList<int>{20,5};

    _rows << QList<int>{20,8};
    _rows << QList<int>{19,10};
    _rows << QList<int>{19,10};
    _rows << QList<int>{16,4,5};
    _rows << QList<int>{16,4,4};

    _rows << QList<int>{13,3,4};
    _rows << QList<int>{13,3,4};
    _rows << QList<int>{14,2,4};
    _rows << QList<int>{15,2,4};
    _rows << QList<int>{16,1,4};

    _rows << QList<int>{16,1,5};
    _rows << QList<int>{18,5};
    _rows << QList<int>{18,6};
    _rows << QList<int>{29,};
    _rows << QList<int>{28,};

    _rows << QList<int>{4,23};
    _rows << QList<int>{5,21};
    _rows << QList<int>{4,17};
    _rows << QList<int>{4,16};
    _rows << QList<int>{4,17};

    _rows << QList<int>{4,4,14};
    _rows << QList<int>{3,4,13};
    _rows << QList<int>{4,3,12};
    _rows << QList<int>{3,3,4,5};
    _rows << QList<int>{3,2,4,5};

    _rows << QList<int>{2,3,4};
    _rows << QList<int>{4,4};
    _rows << QList<int>{5,4};
    _rows << QList<int>{5,5};
    _rows << QList<int>{20,};

    _rows << QList<int>{23,};
    _rows << QList<int>{28,};
    _rows << QList<int>{33,};


    qDebug() << "ROW SIZE IS" << rowsSize();
    qDebug() << "ROW SIZE IS" << colsSize();

}

DataHeader::~DataHeader()
{
}

int DataHeader::rowsSize()
{
    int maxCout = 0;
    int size;

    for(QList<int> row: _rows){
        size = row.size();
        if (size > maxCout)
            maxCout = size;
    }

    return maxCout;
}

int DataHeader::colsSize()
{
    int maxCout = 0;
    int size;

    for(QList<int> row: _cols){
        size = row.size();
        if (size > maxCout)
            maxCout = size;
    }

    return maxCout;
}

