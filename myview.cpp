#include "myview.h"

#include <QMouseEvent>
#include <QPoint>
#include <QDebug>
#include <QWidget>
#include <QScrollBar>


MyView::MyView():QGraphicsView()
{

}

void MyView::mousePressEvent(QMouseEvent *event)
{
    if (Qt::LeftButton == event->button()){
        _pressed = true;
        _lastMousePos = event->pos();
    }
}

void MyView::mouseReleaseEvent(QMouseEvent *event)
{
    if (Qt::LeftButton == event->button())
        _pressed = false;
    QGraphicsView::mouseReleaseEvent(event);
}

void MyView::mouseMoveEvent(QMouseEvent *event)
{
    if (!_pressed)
        return QGraphicsView::mouseMoveEvent(event);

    QPoint diff = _lastMousePos - event->pos();

    if (QScrollBar *hbar = horizontalScrollBar())
        hbar->setValue(hbar->value() + diff.x());

    if (QScrollBar *vbar = verticalScrollBar())
        vbar->setValue(vbar->value() + diff.y());

    _lastMousePos = event->pos();
    return QGraphicsView::mouseMoveEvent(event);

}
