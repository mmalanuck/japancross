#ifndef MYVIEW_H
#define MYVIEW_H

#include <QGraphicsView>

class QPoint;
class QWidget;
class QMouseEvent;
class DataHeader;

class MyView: public QGraphicsView
{
public:
    MyView();

    void mousePressEvent(QMouseEvent *event);
    void mouseReleaseEvent(QMouseEvent *event);
    void mouseMoveEvent(QMouseEvent *event);

private:
    bool _pressed = false;
    QPoint _lastMousePos;
    void makeHeader(DataHeader *data, QGraphicsScene *scene);
};

#endif // MYVIEW_H
