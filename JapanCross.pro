#-------------------------------------------------
#
# Project created by QtCreator 2018-01-26T19:22:31
#
#-------------------------------------------------

QT       += core gui
CONFIG  += c++11

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = JapanCross
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    boxitem.cpp \
    myview.cpp \
    dataheader.cpp \
    crossscene.cpp

HEADERS  += mainwindow.h \
    boxitem.h \
    myview.h \
    dataheader.h \
    crossscene.h
