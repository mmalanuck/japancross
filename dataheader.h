#ifndef DATATABLE_H
#define DATATABLE_H

#include <QObject>

class DataHeader : public QObject
{
    Q_OBJECT
public:
    explicit DataHeader(QObject *parent = 0);
    ~DataHeader();
    QList<QList<int>> _rows;
    QList<QList<int>> _cols;

    /// максимальное количество цифр в строках
    int rowsSize();
    /// максимальное количество цифр в колонках
    int colsSize();

signals:

public slots:
};

#endif // DATATABLE_H
