#ifndef CROSSSCENE_H
#define CROSSSCENE_H

#include <QGraphicsScene>


class DataHeader;
class QPen;

class CrossScene: public QGraphicsScene
{
public:
    CrossScene(QObject *parent = nullptr);
    ~CrossScene();

    void setData(DataHeader *data);
    void makeHeader(int rectSize);
    void buildUI();


private:
    DataHeader *_header;
    QPen pen;
    QPen thickPen;


    void drawHeaderBackground(int rectSize);
    void drawHeaderCols(int rectSize);
    void drawHeaderRows(int rectSize);
    void drawGrids(int rectSize);
    void drawLines(int rectSize);
};

#endif // CROSSSCENE_H
